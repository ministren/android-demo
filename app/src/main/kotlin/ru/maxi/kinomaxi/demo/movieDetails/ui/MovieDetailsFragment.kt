package ru.maxi.kinomaxi.demo.movieDetails.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isVisible
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import kotlinx.datetime.LocalDate
import kotlinx.datetime.format
import ru.maxi.kinomaxi.demo.R
import ru.maxi.kinomaxi.demo.databinding.FragmentMovieDetailsBinding
import ru.maxi.kinomaxi.demo.databinding.LayoutErrorViewBinding
import ru.maxi.kinomaxi.demo.movieDetails.model.MovieDetails
import ru.maxi.kinomaxi.demo.movieDetails.model.MovieGenre
import ru.maxi.kinomaxi.demo.movieDetails.model.MovieImage
import ru.maxi.kinomaxi.demo.setSubtitle
import ru.maxi.kinomaxi.demo.setTitle
import java.util.Locale

class MovieDetailsFragment : Fragment() {

    private var _viewBinding: FragmentMovieDetailsBinding? = null
    private val viewBinding get() = _viewBinding!!

    private val movieId: Long by lazy {
        requireArguments().getLong(MOVIE_ID_ARG_KEY)
    }

    private val viewModel by viewModels<MovieDetailsViewModel>(
        factoryProducer = { MovieDetailsViewModel.createFactory(movieId) }
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _viewBinding = FragmentMovieDetailsBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _viewBinding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(viewBinding) {
            errorView.setOnInflateListener { _, inflated ->
                with(LayoutErrorViewBinding.bind(inflated)) {
                    errorActionButton.setOnClickListener {
                        viewModel.refreshData()
                    }
                }
            }

            movieImagesView.adapter = MovieImagesAdapter()
            moviePosterLayout.favIcon.setOnClickListener {
                viewModel.toggleFavorites()
            }

            ViewCompat.setOnApplyWindowInsetsListener(contentScrollView) { view, windowInsets ->
                val insets = windowInsets.getInsets(WindowInsetsCompat.Type.systemBars())
                view.updatePadding(bottom = insets.bottom)
                WindowInsetsCompat.CONSUMED
            }
        }

        viewModel.setViewStateChangeListener(::showNewState)
    }

    private fun showNewState(state: MovieDetailsViewState) {
        when (state) {
            MovieDetailsViewState.Loading -> with(viewBinding) {
                contentScrollView.isVisible = false
                loaderView.show()
                errorView.isVisible = false
            }

            MovieDetailsViewState.Error -> with(viewBinding) {
                contentScrollView.isVisible = false
                loaderView.hide()
                errorView.isVisible = true
            }

            is MovieDetailsViewState.Success -> with(viewBinding) {
                contentScrollView.isVisible = true
                loaderView.hide()
                errorView.isVisible = false
                showMovieDetails(state.movieDetails)
                showMovieImages(state.movieImages)
            }
        }
    }

    private fun FragmentMovieDetailsBinding.showMovieDetails(movie: MovieDetails) {
        setTitle(movie.title)
        setSubtitle(movie.originalTitle)

        moviePosterLayout.apply {
            val favoriteButtonIconResId = if (movie.isFavorite) {
                R.drawable.ic_favorite_24
            } else {
                R.drawable.ic_favorite_border_24
            }
            favIcon.icon = ContextCompat.getDrawable(requireContext(), favoriteButtonIconResId)

            movieRating.text = String.format(Locale.getDefault(), "%.2f", movie.rating)
            Glide.with(this@MovieDetailsFragment)
                .load(movie.posterImage?.previewUrl)
                .into(moviePoster)
        }

        movieDetailsLayout.apply {
            movieGenres.text = movie.genres.joinToString(
                separator = ", ",
                transform = MovieGenre::name
            )
            movieTitle.text = movie.title
            movieTitleOriginal.text = movie.originalTitle
            movieTagline.text = movie.tagline
            movieReleaseDate.text = movie.releaseDate.format(
                LocalDate.Format { year() }
            )
            movieLength.text = getString(
                R.string.movie_length_value,
                movie.lengthMinutes / 60,
                movie.lengthMinutes % 60
            )
        }

        movieOverview.text = movie.overview
    }

    private fun FragmentMovieDetailsBinding.showMovieImages(movieImages: List<MovieImage>) {
        (movieImagesView.adapter as? MovieImagesAdapter)?.setItems(movieImages)
    }

    companion object {

        const val MOVIE_ID_ARG_KEY = "MOVIE_ID_KEY"

        fun getInstance(movieId: Long) = MovieDetailsFragment().apply {
            arguments = bundleOf(
                MOVIE_ID_ARG_KEY to movieId
            )
        }
    }
}
