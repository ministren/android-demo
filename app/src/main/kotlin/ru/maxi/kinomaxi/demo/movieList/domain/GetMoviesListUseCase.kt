package ru.maxi.kinomaxi.demo.movieList.domain

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.maxi.kinomaxi.demo.movieList.data.MoviesListApiService
import ru.maxi.kinomaxi.demo.movieList.data.MoviesListResponse
import ru.maxi.kinomaxi.demo.movieList.model.Movie
import ru.maxi.kinomaxi.demo.movieList.model.MoviesListType

/**
 * Бизнес-сценарий получения списка фильмов
 */
class GetMoviesListUseCase(
    private val apiService: MoviesListApiService,
) {

    /**
     * Получить список фильмов с типом [listType]
     */
    operator fun invoke(
        listType: MoviesListType,
        onSuccess: (list: List<Movie>) -> Unit,
        onFailure: () -> Unit
    ) {
        val moviesListApiCall = when (listType) {
            MoviesListType.TOP_RATED_MOVIES -> apiService.getTopRatedMovies()
            MoviesListType.POPULAR_MOVIES -> apiService.getPopularMovies()
            MoviesListType.UPCOMING_MOVIES -> apiService.getUpcomingMovies()
        }

        moviesListApiCall.enqueue(object : Callback<MoviesListResponse> {
            override fun onResponse(
                call: Call<MoviesListResponse>,
                response: Response<MoviesListResponse>
            ) {
                if (!response.isSuccessful) {
                    onFailure()
                    return
                }

                val list = response.body()?.movies
                if (list == null) {
                    onFailure()
                    return
                }

                onSuccess(list)
            }

            override fun onFailure(call: Call<MoviesListResponse>, t: Throwable) {
                onFailure()
            }
        })
    }
}
