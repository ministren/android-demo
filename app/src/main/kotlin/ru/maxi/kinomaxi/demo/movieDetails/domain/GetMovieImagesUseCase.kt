package ru.maxi.kinomaxi.demo.movieDetails.domain

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.maxi.kinomaxi.demo.movieDetails.data.MovieDetailsApiService
import ru.maxi.kinomaxi.demo.movieDetails.data.MovieImagesResponse
import ru.maxi.kinomaxi.demo.movieDetails.model.MovieImage

/**
 * Бизнес-сценарий получения списка изображений фильма
 */
class GetMovieImagesUseCase(
    private val apiService: MovieDetailsApiService,
) {

    /**
     * Получить список изображений фильма по идентификатору [movieId]
     */
    operator fun invoke(
        movieId: Long,
        onSuccess: (images: List<MovieImage>) -> Unit,
        onFailure: () -> Unit
    ) {
        apiService.getMovieImages(movieId).enqueue(object : Callback<MovieImagesResponse> {
            override fun onResponse(
                call: Call<MovieImagesResponse>,
                response: Response<MovieImagesResponse>
            ) {
                if (!response.isSuccessful) {
                    onFailure()
                    return
                }

                val images = response.body()?.backdrops
                if (images == null) {
                    onFailure()
                    return
                }

                onSuccess(images)
            }

            override fun onFailure(call: Call<MovieImagesResponse>, t: Throwable) {
                onFailure()
            }
        })
    }
}
