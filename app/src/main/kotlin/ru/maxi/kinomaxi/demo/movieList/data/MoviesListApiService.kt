package ru.maxi.kinomaxi.demo.movieList.data

import retrofit2.Call
import retrofit2.http.GET
import ru.maxi.kinomaxi.demo.createApiService

/**
 * Интерфейс взаимодействия с REST API для функционала списков фильмов
 */
interface MoviesListApiService {

    /**
     * Получить список текущих популярных фильмов
     */
    @GET("movie/popular")
    fun getPopularMovies(): Call<MoviesListResponse>

    /**
     * Получить список фильмов с самым высоким рейтингом
     */
    @GET("movie/top_rated")
    fun getTopRatedMovies(): Call<MoviesListResponse>

    /**
     * Получить список ещё не вышедших фильмов
     */
    @GET("movie/upcoming")
    fun getUpcomingMovies(): Call<MoviesListResponse>

    companion object {
        val instance: MoviesListApiService
            get() = createApiService()
    }
}
