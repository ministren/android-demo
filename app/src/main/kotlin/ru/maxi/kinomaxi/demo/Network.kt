package ru.maxi.kinomaxi.demo

import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.kotlinx.serialization.asConverterFactory

private val httpClient: OkHttpClient = OkHttpClient.Builder()
    .addInterceptor { chain ->
        val url = chain.request().url.newBuilder()
            .addQueryParameter("language", "ru")
            .build()
        val request = chain.request().newBuilder()
            .url(url)
            .build()
        chain.proceed(request)
    }
    .addInterceptor(HttpLoggingInterceptor().apply {
        setLevel(HttpLoggingInterceptor.Level.BODY)
    })
    .build()

private val json = Json { ignoreUnknownKeys = true }
private val jsonMediaType = "application/json; charset=UTF8".toMediaType()

val retrofit: Retrofit = Retrofit.Builder()
    .client(httpClient)
    .baseUrl(AppConfig.API_BASE_URL)
    .addConverterFactory(json.asConverterFactory(jsonMediaType))
    .build()

inline fun <reified T> createApiService(): T {
    return retrofit.create(T::class.java)
}
