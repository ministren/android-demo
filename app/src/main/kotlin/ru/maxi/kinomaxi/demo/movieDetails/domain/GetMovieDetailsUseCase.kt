package ru.maxi.kinomaxi.demo.movieDetails.domain

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.maxi.kinomaxi.demo.favorites.data.FavoriteMoviesRepository
import ru.maxi.kinomaxi.demo.movieDetails.data.MovieDetailsApiService
import ru.maxi.kinomaxi.demo.movieDetails.model.MovieDetails

/**
 * Бизнес-сценарий получения детальной информации о фильме
 */
class GetMovieDetailsUseCase(
    private val apiService: MovieDetailsApiService,
    private val favoriteMoviesRepository: FavoriteMoviesRepository,
) {

    /**
     * Получить детальную информацию о фильме по идентификатору [movieId]
     */
    operator fun invoke(
        movieId: Long,
        onSuccess: (details: MovieDetails) -> Unit,
        onFailure: () -> Unit
    ) {
        apiService.getMovieDetails(movieId).enqueue(object : Callback<MovieDetails> {
            override fun onResponse(
                call: Call<MovieDetails>,
                response: Response<MovieDetails>
            ) {
                if (!response.isSuccessful) {
                    onFailure()
                    return
                }

                val isFavorite = favoriteMoviesRepository.isFavorite(movieId)
                val details = response.body()?.copy(isFavorite = isFavorite)
                if (details == null) {
                    onFailure()
                    return
                }

                onSuccess(details)
            }

            override fun onFailure(call: Call<MovieDetails>, t: Throwable) {
                onFailure()
            }
        })
    }
}
