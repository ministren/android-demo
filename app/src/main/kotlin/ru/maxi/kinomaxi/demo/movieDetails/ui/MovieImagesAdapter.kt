package ru.maxi.kinomaxi.demo.movieDetails.ui

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.maxi.kinomaxi.demo.databinding.ItemMovieImageBinding
import ru.maxi.kinomaxi.demo.movieDetails.model.MovieImage

/**
 * Адаптер для списка изображений фильма
 */
class MovieImagesAdapter : RecyclerView.Adapter<MovieImageViewHolder>() {

    private val items = mutableListOf<MovieImage>()

    @SuppressLint("NotifyDataSetChanged")
    fun setItems(items: List<MovieImage>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieImageViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemMovieImageBinding.inflate(layoutInflater, parent, false)
        return MovieImageViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MovieImageViewHolder, position: Int) {
        val data = items[position]
        holder.bind(data)
    }
}
